using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace WindowsGame1
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Cible : Movable
    {
       
        Vector2 depart;
        Vector2 destination;

        public Cible(Game game, string texture, Vector2 d1, Vector2 d2 )

            : base(game, texture)
        {
            // TODO: Construct any child components here
            depart = d1;
            destination = d2;

            setTrajectory();

          
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public void setTrajectory ()
        {
            // TODO: Add your update code here
            int i=0;
            float deltaY = destination.Y-depart.Y;
            float deltaX = destination.X - depart.X;
            float coefD = deltaY / deltaX;
            float B = destination.Y - coefD * destination.X;
            if (depart.X < destination.X)
            {


                for (float x = depart.X; x < destination.X; x++)
                {

                    float y;
                    y = coefD * x + B;
                    setOfPoints[i] = new Vector2(x, y);
                    i++;

                }
            }
            else
            {

                for (float x = destination.X; x < depart.X; x++)
                {

                    float y;
                    y = coefD * x + B;
                    setOfPoints[i] = new Vector2(x, y);
                    i++;

                }

            }
        }

        public Texture2D depat { get; set; }
        
    }
}
